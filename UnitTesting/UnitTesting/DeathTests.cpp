#include <gtest/gtest.h>

void exitFunction()
{
	std::cerr << "Bad thing happened";
	std::exit(1);
}

TEST(tut_DeathTest, simpleTest)
{
	// Death: Non-zero exit pr prog killed by signal
	EXPECT_DEATH(exitFunction(), "Bad thing.*");

	EXPECT_EXIT(exitFunction(), testing::ExitedWithCode(1), "Bad thing.*");
	//EXPECT_EXIT(exitFunction(), testing::KilledBySignal(SIGKIL), "Bad thing.*"); Not suported on windows
}