#include <gtest/gtest.h>
#include <stack>

class MyStackTest : public testing::Test
{
protected:
	void SetUp() override
	{
		stack.push(34);
		stack.push(28);
		stack.push(56);
	}

	void TearDown() override
	{
		
	}

	std::stack<int> stack;
};

TEST_F(MyStackTest, testPop)
{
	auto val = stack.top();
	stack.pop();

	EXPECT_EQ(56, val);
}

TEST_F(MyStackTest, testPop2)
{
	auto val = stack.top();
	stack.pop();

	EXPECT_EQ(56, val);
}