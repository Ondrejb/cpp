#include <gtest/gtest.h>
#include <stack>

TEST(StackTest, testPop)
{
	std::stack<int> stack;
	stack.push(9);
	EXPECT_EQ(9, stack.top());
}

TEST(StackTest, testAll)
{
	std::stack<int> stack;

	stack.push(9);
	stack.push(28);

	auto val = stack.top();
	stack.pop();

	// Nonfatal assertion - if assertion fails, next one will be evaluated
	EXPECT_EQ(28, val);
	EXPECT_NE(0, val);	// 0 == val
	EXPECT_GT(29, val);	// 29 > val
	EXPECT_LE(27, val);	// 27 <= val
	EXPECT_TRUE(val == 28) << "val somehow is not equal to 28"; // Custom msg

	// Fatal assertion - if assertion fails, others will not be evaluated
	ASSERT_EQ(28, val);

	// Strings
	EXPECT_STREQ("28", std::to_string(val).c_str());
	EXPECT_STRCASEEQ("AHOJ", "ahoj"); // ignore case

	// Floating point comparison
	float x, y;
	EXPECT_FLOAT_EQ(7.0, static_cast<float>(val) / 4);	// within 4 ULP's from each other
														// ULP - Unit in the Last Place
														// ULP = X - Y, where Y is the small float that is less than X
	EXPECT_DOUBLE_EQ(5.6, static_cast<double>(val) / 5);
	EXPECT_NEAR(6.0, static_cast<double>(val) / 5, 1);	// custom tolerance
}