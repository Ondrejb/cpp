#include <gtest/gtest.h>
#include <stack>

// Will not be run (DISABLED_...)
TEST(StackTest, DISABLED_testPush)
{
	std::stack<int> s;
	s.push(1);
	EXPECT_EQ(s.size(), 1);
}

// Command Args can be used to disable tests as Well
// --gtest_filter=StackTest.*
// all tests in StackTest case will be skiped