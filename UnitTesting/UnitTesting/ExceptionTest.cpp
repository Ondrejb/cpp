#include <gtest/gtest.h>

TEST(tut_ExceptionTest, simpleTest)
{
	std::vector<int> v(5);
	
	EXPECT_THROW(v.at(10) = 3, std::out_of_range);
}

TEST(tut_Fail, failTest)
{
	if (1 + 1 != 2)
		FAIL();
}
