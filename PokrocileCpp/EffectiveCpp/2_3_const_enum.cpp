#define NUM_TURNS 5					// bad (encapsulation, compiler won't know about it, ...)
#include <string>
#include <iostream>

class EnumHack
{
	static const int NumTurns = 5;	// bad compilers might not allow this
	enum { NumTurnsE = 5 };			// that's when the enum hack comes in

	int scores[NumTurns];
	int scoresE[NumTurnsE];
};

void f(int a, int b);

#define CALL_WITH_MAX(a, b) f((a)>(b) ? (a) : (b))	// bad, param might get evaluated twice 
													// CALL_WITH_MAX(++a, b) - if a was greater, it would be incremented twice

// better to use template
template<class T>
inline void callWithMax(const T& lhs, const T& rhs)
{
	f(lhs > rhs ? lhs : rhs);
}

class Text
{
	std::string str;

	mutable std::size_t length;
	mutable bool isLengthValid;
public:
	
	explicit Text(std::string text)
	{
		str = text;
		length = text.length();
		isLengthValid = true;
	}

	const char& operator[](std::size_t position) const
	{
		if(!isLengthValid)
		{
			length = str.length();	// wouldn't be possible if non-mutable
			isLengthValid = true;	// ditto
		}

		if (position >= length)
			throw std::out_of_range("Position is out of range");

		// do loggin and other stuff

		return str[position];
	}

	char& operator[](std::size_t position)
	{
		// to avoid duplicity, we'll call the cost version
		return const_cast<char&>							// remove the const qualifier
			(std::as_const(*this)[position]);				// make this const so that the const operator[] is called (C++17 way)
			//	(const_cast<const Text&>(*this)[position]);	// same using const_cast
			//	(static_cast<const Text&>(*this)[position]);// same using static_cast
	}
};

int main23()
{
	Text t1("Text 1");
	std::cout << t1[2] << "\n";

	const Text t2("Const text");
	std::cout << t2[3] << "\n";

	t1[0] = 'A';	// OK, is non-const
	// t2[0] = 'A';	// can't

	try
	{
		t1[20] = 'X';	// out of range
	}
	catch(std::exception& ex)
	{
		std::cout << ex.what() << "\n";
	}

	return 0;
}