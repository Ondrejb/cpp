#include <iostream>

class Widget
{
public:
	int a;

	explicit Widget(int a)
	{
		this->a = a;
	}

	// copy ctor
	Widget(const Widget& rhs)
	{
		this->a = rhs.a;		
	}

	// copy assignment operator
	/*
	Widget& operator=(const Widget& rhs)
	{
		this->a = rhs.a;
		return *this;
	}
	*/

	// ref return is a convention, supports chain assignments
	// copy and swap - makes assignment exception- and self-assignment safe
	Widget& operator=(Widget rhs)
	{
		// rhs is a copy, so the rhs original won't be effected by the swap
		std::swap(this->a, rhs.a);
		return *this;
	}
};

void printWidget(Widget w)
{
	std::cout << w.a << "\n";
}

template<class T>
class NamedObject
{
	std::string& _name;
	const T _objectValue;
public:
	NamedObject(std::string& name, const T& object)
		: _name(name), _objectValue(object)
	{
		log();	// log is virtual -> bad, would be called even from derived classes
				// logging information should be passed from derived classes
				// as a ctor param (created by a static method in the derived class)
	}

	NamedObject<T>& operator=(NamedObject rhs) = delete;

	virtual void log() { std::cout << "Log entry" << "\n"; }
};

int main04()
{
	Widget w1(1);
	Widget w2(w1);			// copy ctor
	w2.a = 5;
	w1 = w2;				// invoke copy assignment operator
	Widget w3 = w2;			// copy ctor

	printWidget(w1);		// ok, will call copy ctor
	// printWidget(10);		   won't work, we are using explicit
	printWidget(Widget(10));// OK, explicit cast


	std::string zolik("Zolik");
	std::string art("Art");

	NamedObject<int> zolikObject(zolik, 13);
	NamedObject<int> artObject(art, 1);

	NamedObject<int> zolikCopy(zolikObject);

	zolik = "Best dog";

	// artObject = zolikObject;
	NamedObject<int> artCopy = artObject;

	return 0;
}