#include <algorithm>

namespace widget
{
	template<typename T>
	class Widget
	{
		// ...

	public:
		void swap(Widget<T>& other) noexcept
		{
			// ...
		}
	};

	template<typename T>
	void swap(Widget<T>& lhs, Widget<T>& rhs)
	{
		lhs.swap(rhs);
	}
}

template<typename T>
void doSomething(T& obj1, T& obj2)
{
	// ...

	using std::swap;
	int a = 1;
	int b = 5;

	// calls std swap
	swap(a, b);
	
	// calls widget::swap
	swap(obj1, obj2);
}

int main25()
{
	widget::Widget<int> w1, w2;

	doSomething(w1, w2);

	return 0;
}