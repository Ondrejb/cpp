
class Rational
{
public:
	// not explicit, we want implicit conversion
	Rational(int numerator = 0, int denominator = 1)
	: _numerator(numerator), _denominator(denominator)
	{ }

	const int getNumerator() const { return _numerator; }
	const int getDenominator() const { return _denominator; }

private:
	int _numerator;
	int _denominator;
};

const Rational operator*(const Rational& lhs, const Rational& rhs)
{
	return Rational(
		lhs.getNumerator() * rhs.getNumerator(), 
		lhs.getDenominator() * rhs.getDenominator());
}

int main24()
{
	Rational oneFourth(1, 4);
	Rational result;

	result = 2 * oneFourth;

	return 0;
}