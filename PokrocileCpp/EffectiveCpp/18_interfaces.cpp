#include <cstdint>
#include <cassert>

struct Month
{
	static Month January() { return Month(1); }

	uint8_t getVal() const { return _val; }

private:
	explicit Month(uint8_t val) : _val(val) {};

	const uint8_t _val;
};

struct Day
{
	explicit Day(uint8_t val) : _val(val)
	{
		assert(val <= 31, "Day needs to be <= 31");
	}

	uint8_t getVal() const { return _val; }

private:
	const uint8_t _val;
};

struct Year
{
	explicit Year(int val) : _val(val) {};

	int getVal() const { return _val; }

private:
	const int _val;
};

class Date
{
public:
	Date(Year year, Month month, Day day)
		: _year(year), _month(month), _day(day)
	{

	}

// getter functions, operators etc.


private:
	const Year _year;
	const Month _month;
	const Day _day;
};

int main18()
{
	// By using stucts for year, month and day, we make the Date interface hard to use incorrectly
	Date date(Year(2017), Month::January(), Day(24));
	Date date2(Year(2017), Month::January(), Day(50));

	return 0;
}