#pragma once

#include <iterator>

template<typename IterT>		// class could be used here
void workWithIter(IterT iter)
{
	// have to use typename, because value_type is a nested type in the iterator class
	// typedef is used so that we don't have to type the whole thing every time
	typedef typename std::iterator_traits<IterT>::value_type value_type;

	value_type temp(*iter);

	// ...
}