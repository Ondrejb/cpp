#include <utility>

int main51()
{
	auto fib = [a = 0, b = 1]() mutable {
		struct Results
		{
			int &a;
			int &b;

			Results next(int num = 1) const
			{
				while (num > 0) 
				{
					a = std::exchange(b, b + a);
					--num;
				}
			
				return *this;
			}

			operator int() const
			{
				return a;
			}
		};
	
		return Results{ a, b }.next();
	};

	fib();
	fib();
	fib();
	fib();

	int i = fib().next(5);
	return i;
}
